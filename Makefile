output: main.o lib_calculator.a lib_dynamic.so
	g++ -o output main.o -L. -l_calculator -l_dynamic -Wl,-rpath,/home/emumba/Documents/gcc

main.o: main.cpp
	g++ -Wall -c main.cpp

divide.o: divide.cpp
	g++ -Wall -c divide.cpp

multiply.o: multiply.cpp
	g++ -Wall -c multiply.cpp

subtract.o: subtract.cpp
	g++ -Wall -c subtract.cpp

add.o: add.cpp
	g++ -Wall -c add.cpp

lib_calculator.a: divide.o multiply.o subtract.o add.o
	ar rcs lib_calculator.a divide.o multiply.o subtract.o add.o

lib_dynamic.o: lib_dynamic.cpp
	g++ -Wall -c lib_dynamic.cpp

lib_dynamic.so: lib_dynamic.o
	g++ -shared -fPIC -o lib_dynamic.so lib_dynamic.cpp
	
clean:
	rm *.o *.a *.so output