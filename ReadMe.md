# GCC TASKS

## ABOUT

Calculator library consists of following .cpp files
- add.cpp
- subtract.cpp
- multiply.cpp
- divide.cpp

## How to Run

- To compile object files and exeutable file run **make**
- To run executable file **./output**
- to clean .o and executable files run **make clean**

